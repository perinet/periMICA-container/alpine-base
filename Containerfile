# Copyright Perinet GmbH. All Rights Reserved.

FROM --platform=$BUILDPLATFORM docker.io/library/golang:1.22-alpine3.20 AS builder

ARG TARGETOS
ARG TARGETARCH

RUN apk add git upx

RUN git clone --depth 1 --branch 1.1 https://gitlab.com/perinet/periMICA-container/avahi2dns.git /usr/local/go/src/avahi2dns
WORKDIR /usr/local/go/src/avahi2dns
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -gcflags=all="-l -B" -ldflags="-w -s"
# high compression rates might provoke false positives with windows defender
# ticket:MICACNTNR-142
# RUN upx --best --lzma avahi2dns
RUN upx -9 avahi2dns


FROM --platform=$TARGETPLATFORM docker.io/library/alpine:3.21

# needed for successful target setting
ARG TARGETPLATFORM

LABEL io.perinet.image.is-system-container="true"
LABEL io.perinet.image.init="/sbin/init"

COPY rootfs/ /

COPY --from=builder /usr/local/go/src/avahi2dns/avahi2dns /usr/bin/

RUN apk --no-cache add alpine-base logrotate logrotate-openrc
RUN apk --no-cache add sudo avahi avahi-tools dbus libcap2 openssl && \
    rm -rf /etc/avahi/services/*.service 

RUN rc-update add dev-init boot &&\
    rc-update add crond default &&\
    rc-update add dbus default &&\
    rc-update add avahi-daemon default &&\
    rc-update add avahi2dns default

RUN apk -q list --installed

CMD [ "/sbin/init" ]
